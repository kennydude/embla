from django.contrib import admin

from embla.base.models import *


@admin.register(Organisation)
class OrganisationAdmin(admin.ModelAdmin):
    pass


class ModuleLinkAdmin(admin.TabularInline):
    model = ModuleUser


@admin.register(Module)
class ModuleAdmin(admin.ModelAdmin):
    inlines =  [
        ModuleLinkAdmin
    ]


@admin.register(Year)
class YearAdmin(admin.ModelAdmin):
    pass
