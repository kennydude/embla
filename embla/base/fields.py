import json

from django.db import models
from django.forms.widgets import Textarea
from django.template.loader import render_to_string
from django.forms.utils import flatatt


class JSONField(models.TextField):
    """A text field containing valid JSON
    """

    def from_db_value(self, value, *args):
        """Deserialize
        """
        return json.loads(value)

    def to_python(self, value):
        """Make pythonic
        """
        if isinstance(value, dict) or isinstance(value, list):
            return value
        return json.loads(value)

    def get_prep_value(self, value):
        """Serialize
        """
        return json.dumps(value)


class KeyValueWidget(Textarea):
    """A widget which will render a key-value editor
    """
    def render(self, name, value, attrs=None, **kwargs):
        """Render to template
        """
        attrs = self.build_attrs(attrs, name=name)
        return render_to_string('base/key_value.html', {
            'value': json.dumps(value),
            'attrs': flatatt(attrs),
            'id': attrs['id']
        })


class KeyValueField(JSONField):
    """Key-Value Field
    """

    def formfield(self, **kwargs):
        """Returns a Key-Value Field to render with
        """
        kwargs.update({'widget': KeyValueWidget})
        return super(KeyValueField, self).formfield(**kwargs)
