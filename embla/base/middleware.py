import logging
from datetime import datetime

from django.shortcuts import redirect
from django.shortcuts import resolve_url
from django.contrib.auth.views import redirect_to_login

from embla.base.models import Organisation


logger = logging.getLogger(__name__)


class CurrentOrganisationMiddleware(object):
    """Sets the current organisation based on the
    request
    """

    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        try:
            request.organisation = Organisation.objects.get(
                domain=request.get_host())
        except Organisation.DoesNotExist:
            request.organisation = None
            logging.warn(
                '% does not have an organisation', request.get_host())

        request.today = datetime.utcnow()

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response


class LoginRequiredMiddleware(object):
    """Require login
    """
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        if request.user.is_anonymous:
            if ('/login' not in request.path_info and
                    '/static' not in request.path_info and
                    '/media' not in request.path_info):
                resolved_login_url = resolve_url('login')

                return redirect_to_login(
                    request.path_info, resolved_login_url, 'next')

        response = self.get_response(request)
        return response
