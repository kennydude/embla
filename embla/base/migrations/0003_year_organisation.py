# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-13 01:54
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0002_auto_20170311_2200'),
    ]

    operations = [
        migrations.AddField(
            model_name='year',
            name='organisation',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='base.Organisation'),
            preserve_default=False,
        ),
    ]
