from rest_framework.response import Response


class RetrieveMetadaMixin(object):
    """Adds metadata to retrieve response
    """

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        data = serializer.data
        data.update({
            "meta": self.get_metadata()
        })
        return Response(data)
