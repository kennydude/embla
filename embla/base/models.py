import datetime
import random

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from embla.base.fields import KeyValueField


class Organisation(models.Model):
    """An organisation is typically something like Newcastle University
    which would be configured on embla.ncl.ac.uk
    """
    name = models.CharField(max_length=200)
    domain = models.CharField(max_length=200)
    authentication = KeyValueField()
    icon = models.FileField()
    config = KeyValueField()
    submission_regulations = models.TextField(
        blank=True, help_text='''
Markdown of the university regulations neeed to be displayed in
the Full Information dropdown of submitting coursework''')

    def __str__(self):
        return '{} @ {}'.format(self.name, self.domain)


class YearManager(models.Manager):
    """Chstom manager for academic years
    """

    def get_current(self, organisation):
        """Get the current year for the organisation specfiiced
        """
        now = datetime.date.today()
        return self.get(organisation=organisation, start__lte=now, end__gte=now)


class Year(models.Model):
    """An academic year
    """
    organisation = models.ForeignKey(Organisation)
    display = models.CharField(max_length=50)
    start = models.DateField()
    end = models.DateField()

    objects = YearManager()

    def __str__(self):
        return self.display


def random_color():
    """Generate a random (darker) colour for use on the sidebar
    """
    r = lambda: random.randint(0, 150)
    return '#%02X%02X%02X' % (r(), r(), r())


class Module(models.Model):
    """A module, which is part of a course a student is enrolled on
    """
    code = models.CharField(max_length=50)
    name = models.CharField(max_length=200)

    description = models.TextField()

    year = models.ForeignKey(Year)
    users = models.ManyToManyField(settings.AUTH_USER_MODEL, through='ModuleUser')
    image = models.ImageField(null=True)
    color = models.CharField(max_length=7, default=random_color)

    def __str__(self):
        return '{} {} {}'.format(self.code, self.name, self.year)


class ModuleUser(models.Model):
    """Relationship between a Module and a User
    """
    RELATIONSHIP_STUDENT = 'student'
    RELATIONSHIP_MARKER = 'marker'
    RELATIONSHIP_STAFF = 'staff'

    RELATIONSHIP_TYPES = (
        (RELATIONSHIP_STUDENT, _('Student')),
        (RELATIONSHIP_MARKER, _('Marker')),
        (RELATIONSHIP_STAFF, _('Staff Member')),
    )

    module = models.ForeignKey(Module)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    relationship = models.CharField(
        max_length=50, choices=RELATIONSHIP_TYPES)
