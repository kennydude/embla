from rest_framework import pagination
from rest_framework.response import Response
from collections import OrderedDict


class EmblaPagination(pagination.PageNumberPagination):
    """Customised pagination which adds metadata
    """

    def paginate_queryset(self, queryset, request, view=None):
        self.view = view
        return super(EmblaPagination, self).paginate_queryset(queryset, request, view)

    def get_paginated_response(self, data, view=None):
        meta = None
        if view is not None:
            self.view = view

        out = {
            'results': data
        }
        if hasattr(self, 'page'):
            out['count'] = self.page.paginator.count
            out['next'] = self.get_next_link()
            out['previous'] = self.get_previous_link()

        if hasattr(self, 'view'):
            if hasattr(self.view, 'get_metadata'):
                out['meta'] = getattr(self.view, 'get_metadata')()

        return Response(OrderedDict(out))
