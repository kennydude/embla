from django.contrib.auth.models import User

from rest_framework import serializers

from embla.base.models import Module, ModuleUser


class ModuleSerializer(serializers.ModelSerializer):
    year = serializers.CharField(source='year.display')
    user_role = serializers.SerializerMethodField()

    class Meta:
        model = Module
        fields = [
            'id', 'color', 'name', 'code', 'year', 'image', 'user_role', 'description',]

    def get_user_role(self, obj):
        mu = ModuleUser.objects.get(
            user=self.context['request'].user, module=obj)
        return mu.relationship


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name',]


class ModuleLinkSerializer(serializers.ModelSerializer):
    """Similar to UserSerializer, but includes role
    """
    id = serializers.IntegerField(source='user.id')
    first_name = serializers.CharField(source='user.first_name')
    last_name = serializers.CharField(source='user.last_name')
    user_role = serializers.CharField(source='relationship')

    class Meta:
        model = ModuleUser
        fields = ['id', 'first_name', 'last_name', 'user_role']
