function run(el){
    // Load in sections
    $(".load-page", el).each(function(){
        $(this).load($(this).attr("data-url"), function(){
            run($(this));
        }).removeClass("load-page");
    });
    // Checkbox accordion
    $(".multiple-choice input", el).change(function(){
        $("input", $(this).closest("fieldset")).each(function(){
            let checked = $(this).is(":checked");
            let el =  $("#" + $(this).parent().attr("data-target"));
            if(checked){
                el.show();
            } else{
                el.hide();
            }
        });
    }).trigger("change");
    // Array of inputs
    $(".string-array", el).each(function(){
        $("input", this).each(function(){
            addSmallDelete(this);
        });
        $("<input class='form-control'>").attr(
            "placeholder", "Add new item").appendTo(this).on("focus", function(){
                let i = $("<input class='form-control'>").insertBefore(this).focus();
                addSmallDelete(i);
        });
    });
    // Student Selector
    $(".student-selector").each(function(){
        let val = $(this);
        let selected = $("<p class='font-xsmall'>").insertAfter(this);
        let input = $(
            "<input class='form-control' placeholder='"+trans['select_student']+"' type=text />"
        ).autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: val.data("url"),
                    data: {"search": request.term},
                    success: function(data){
                        response(data['results']);
                    }
                });
            },
            focus: function(event, ui){
                input.val(ui.item.first_name + " " + ui.item.last_name);
            },
            select: function(event, ui){
                let name = ui.item.first_name + " " + ui.item.last_name;
                selected.text("Selected " + name + " ");
                input.hide();
                $("<a href='#'>").text("change?").appendTo(selected).click(function(e){
                    e.preventDefault();
                    input.show().focus();
                });
                input.val(name);
                val.val(ui.item.id);
            },
            open: function(){
                $('.ui-autocomplete').css('max-width', '400px');
            }
        }).insertAfter(selected).attr({
            "autocomplete": "off",
            "autocorrect": "off",
            "autocapitalize": "off",
            "spellcheck": "false"
        });
        input.autocomplete( "instance" )._renderItem = function(ul, item) {
            return $("<li>").append($("<div>").text(
                item.first_name + " " + item.last_name)).appendTo( ul );
        };
    });
    $(".tabs").each(function(){
        if($(this).is(".processed")){
            return;
        }
        $(this).addClass("processed");
        let tabs = this;
        let contents = $(this).next(".tab-content");
        console.log(contents);
        $("a", this).click(function(e){
            if(!$(this).is(".normal")){
                e.preventDefault();
                contents.html('').load($(this).attr("href"), function(){
                    run(contents);
                });
            }
            $(".active", tabs).removeClass("active");
            $(this).parent().addClass("active");
        });
        $(".active a", this).click();
    });
    $(document).trigger("run");
}

function addSmallDelete(i){
    $("<a class='small-delete'>").html("<span>&times;</span> Remove").insertAfter(i).click(function(){
        $(this).remove();
        i.remove();
    });
}

run(document.body);

// CSRF
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});
