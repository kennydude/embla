from django import template
from django.utils.safestring import mark_safe

import CommonMark


register = template.Library()


@register.filter()
def markdown(value):
    """Converts input into Markdown
    """
    parser = CommonMark.Parser()
    ast = parser.parse(value)

    renderer = CommonMark.HtmlRenderer({
        'safe': True
    })
    html = renderer.render(ast)
    return mark_safe(html)
