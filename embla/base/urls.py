from django.conf.urls import url
from django.views.generic import TemplateView
from django.contrib.auth import views as auth_views

from embla.base.views import (
    ModuleViewSet, ModuleUsersViewSet)

urlpatterns = [
    url(r'^$', TemplateView.as_view(
        template_name='base/index.html'
    ), name='login'),

    # Authentication
    url(r'^login/$', TemplateView.as_view(
        template_name='base/login.html'
    ), name='login'),
    url(r'^login/password/$', auth_views.login, name='login-password'),
    url(r'^accounts/logout/$', auth_views.logout, name='logout'),

    # modules
    url(r'^modules/$',
        ModuleViewSet.as_view({
            'get': 'list'
        }), name='modules'),
    url(r'^modules/(?P<pk>[0-9]+)/$',
        ModuleViewSet.as_view({
            'get': 'retrieve'
        }, template_name='base/module.html'), name='module-single'),
    url(r'^modules/(?P<module_id>[0-9]+)/users/$',
        ModuleUsersViewSet.as_view({
            'get': 'list'
        }), name='module-user-list'),
]
