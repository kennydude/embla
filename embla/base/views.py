from rest_framework import viewsets
from rest_framework import filters

import django_filters
from django_filters.rest_framework import DjangoFilterBackend

from embla.base.models import Module, Year, ModuleUser
from embla.base.serializers import (
    ModuleSerializer, UserSerializer, ModuleLinkSerializer)


class ModuleViewSet(viewsets.ReadOnlyModelViewSet):
    """
    list:

    Lists the modules the current user has acccess to
    """

    serializer_class = ModuleSerializer
    template_name = 'base/modules.html'

    def get_queryset(self):
        return Module.objects.filter(
            year=Year.objects.get_current(self.request.organisation),
            users__in=[self.request.user])


class ModuleUsersFilterSet(django_filters.FilterSet):
    user_role = django_filters.ChoiceFilter(
        name='relationship', choices=ModuleUser.RELATIONSHIP_TYPES)

    class Meta:
        model = ModuleUser
        fields = ['user_role',]


class ModuleUsersViewSet(viewsets.ReadOnlyModelViewSet):
    """
    list:

    Lists the users attached to a module
    """
    template_name = 'base/user_list.html'
    serializer_class = ModuleLinkSerializer
    filter_class = ModuleUsersFilterSet
    search_fields = ('user__first_name', 'user__last_name', )
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, )

    def get_queryset(self):
        return ModuleUser.objects.filter(module_id=self.kwargs['module_id'])
