from django.contrib import admin

from embla.coursework.models import *


@admin.register(MarkScheme)
class MarkSchemeAdmin(admin.ModelAdmin):
    pass


class CourseworkPartAdmin(admin.TabularInline):
    model = CourseworkPart


@admin.register(Coursework)
class CourseworkAdmin(admin.ModelAdmin):
    inlines = [
        CourseworkPartAdmin
    ]


class SubmissionAttachmentAdmin(admin.TabularInline):
    model = SubmissionAttachment


@admin.register(Submission)
class SubmissionAdmin(admin.ModelAdmin):
    inlines = [
        SubmissionAttachmentAdmin
    ]


@admin.register(SubmissionAttachment)
class AttachmentAdmin(admin.ModelAdmin):
    pass


@admin.register(Mark)
class MarkAdmin(admin.ModelAdmin):
    pass
