from datetime import datetime

import arrow

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.core import signing

from embla.base.models import Module, Organisation
from embla.base.fields import JSONField, KeyValueField


class MarkScheme(models.Model):
    """Mark scheme
    """
    organisation = models.ForeignKey(Organisation)
    title = models.CharField(max_length=100)
    late_cap_percentage = models.IntegerField()
    late_days = models.DurationField()
    pass_mark_percentage = models.IntegerField()


class Coursework(models.Model):
    """A piece of coursework which is part of a module
    """
    title = models.CharField(max_length=200)
    module = models.ForeignKey(Module)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL)

    max_mark = models.IntegerField()
    mark_scheme = models.ForeignKey(MarkScheme)

    description = models.TextField()
    description_is_link = models.BooleanField(default=False)

    # Dates
    visible = models.DateTimeField()
    submissions_open = models.DateTimeField()
    due = models.DateTimeField()
    submissions_close = models.DateTimeField()
    mark_by = models.DateTimeField()
    returned = models.DateTimeField()


class CourseworkPart(models.Model):
    """A part of the coursework submission
    """
    TYPE_ONLINE = 'online'
    TYPE_OFFLINE = 'offline'
    TYPE_SLOT = 'slot'

    TYPES = (
        (TYPE_ONLINE, _('Online Submission')),
        (TYPE_OFFLINE, _('Offline Submission')),
        (TYPE_SLOT, _('Slot Booking')),
    )

    SUBMISSION_GROUP = 'group'
    SUBMISSION_INDIVIDUAL = 'individual'
    SUBMISSION_ANONYMOUS = 'anon'

    SUBMISSION_ENTITIES = (
        (SUBMISSION_GROUP, _('Group Submission')),
        (SUBMISSION_INDIVIDUAL, _('Individual Submission')),
        (SUBMISSION_ANONYMOUS, _('Anonymous Individual Submission')),
    )

    coursework = models.ForeignKey(Coursework, related_name='parts')
    part_type = models.CharField(
        max_length=50, choices=TYPES)
    submission_entity = models.CharField(
        max_length=50, choices=SUBMISSION_ENTITIES)
    description = models.TextField(blank=True)
    details = JSONField()


class Submission(models.Model):
    """A submission for a part of coursework
    """
    STATE_LATE = 'late'
    STATE_ON_TIME = 'on time'

    STATES = (
        (STATE_LATE, _('Late')),
        (STATE_ON_TIME, _('On Time')),
    )

    part = models.ForeignKey(CourseworkPart)
    student = models.ForeignKey(settings.AUTH_USER_MODEL)
    submitted = models.DateTimeField(null=True)
    current_submission = models.BooleanField(default=True)

    def state(self):
        """Returns the state of the submission
        """
        if self.submitted > self.part.coursework.due:
            return self.STATE_LATE
        return self.STATE_ON_TIME

    def receipt(self):
        """Generates a receipt code
        """
        code = 'P{}-{}-{}'.format(
            self.part_id,
            arrow.get(self.submitted).timestamp,
            '-'.join([str(p.id) for p in self.attachments.all()]))
        signer = signing.Signer(sep='|')
        return signer.sign(code).replace("|", "-")

    def __str__(self):
        return '{} {} {} {}'.format(
            self.part, self.student, self.submitted, str(self.current_submission))


class SubmissionAttachment(models.Model):
    """An attachment on a submission
    """
    submission = models.ForeignKey(
        Submission, null=True, related_name='attachments')
    attachment = models.FileField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    created = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return str(self.attachment)


class Mark(models.Model):
    """A mark for a piece of coursework
    """
    MARK_PASS = 'pass'
    MARK_COMPENSATED_PASS = 'comp'
    MARK_FAIL = 'fail'

    MARK_TYPES = (
        (MARK_PASS, _('Pass Mark')),
        (MARK_COMPENSATED_PASS, _('Compensatable Pass Mark')),
        (MARK_FAIL, _('Fail Mark')),
    )

    coursework = models.ForeignKey(Coursework)
    student = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='marks_for')
    mark = models.IntegerField()
    mark_type = models.CharField(max_length=10, choices=MARK_TYPES)
    marker = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='marked')
    comments = KeyValueField()
    returned = models.BooleanField(default=False)
