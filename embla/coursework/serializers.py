from django.contrib.auth.models import User
from django.utils.timezone import now

from rest_framework import serializers

from embla.coursework.models import (
    Mark,
    Coursework, MarkScheme, CourseworkPart, Submission, SubmissionAttachment)


COURSEWORK_BASIC_FIELDS = [
    'id', 'module_id', 'title', 'description', 'description_is_link', 'visible',
    'submissions_open', 'due', 'submissions_close', 'mark_by',
    'returned', 'max_mark', ]


class CourseworkSerializer(serializers.ModelSerializer):
    mark = serializers.SerializerMethodField()

    def get_mark(self, obj):
        mark = Mark.objects.filter(
            coursework=obj, returned=True, student=self.context['request'].user)
        if mark.exists():
            return MarkSerializer(mark[0]).data

    class Meta:
        model = Coursework
        fields = COURSEWORK_BASIC_FIELDS + ['mark', ]


class CourseworkPartSerializer(serializers.ModelSerializer):
    details = serializers.JSONField()

    class Meta:
        model = CourseworkPart
        fields = ['id', 'part_type', 'submission_entity', 'description', 'details']


class FullCourseworkSerializer(serializers.ModelSerializer):
    parts = CourseworkPartSerializer(many=True)

    class Meta:
        model = Coursework
        fields = COURSEWORK_BASIC_FIELDS + ['owner', 'parts', 'mark_scheme', 'mark',]

    def create(self, validated_data):
        # Save entire coursework blob
        parts_data = validated_data.pop('parts')
        coursework = Coursework.objects.create(
            module_id=self.context['request'].resolver_match.kwargs['module_id'], **validated_data)
        for part in parts_data:
            CourseworkPart.objects.create(coursework=coursework, **part)
        return coursework

    mark = serializers.SerializerMethodField()

    def get_mark(self, obj):
        mark = Mark.objects.filter(
            coursework=obj, returned=True, student=self.context['request'].user)
        if mark.exists():
            return MarkSerializer(mark[0]).data

    def update(self, instance, validated_data):
        # Update enitre coursework blob
        parts_data = validated_data.pop('parts')
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()

        ids = []
        for part in parts_data:
            if 'id' in part:  # Update existing
                ids.append(id)
                CourseworkPart.objects.filter(
                    id=part['id'], coursework=instance).update(**part)
            else:
                part = CourseworkPart.objects.create(coursework=instance, **part)
                ids.append(part.id)
        CourseworkPart.objects.filter(coursework=instance).exclude(id__in=ids).delete()

        return instance


class MarkSchemeSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarkScheme
        fields = ['id', 'title', 'late_cap_percentage', 'late_days', 'pass_mark_percentage']


class AttachmentSerialzer(serializers.ModelSerializer):
    class Meta:
        model = SubmissionAttachment
        fields = ['id', 'attachment']


class SubmissionSerializer(serializers.ModelSerializer):
    attachments = AttachmentSerialzer(many=True)
    submitted = serializers.DateTimeField(read_only=True)
    student = serializers.PrimaryKeyRelatedField(
        required=False, queryset=User.objects.all())
    part = serializers.PrimaryKeyRelatedField(
        read_only=True)

    class Meta:
        model = Submission
        fields = ['id', 'part', 'student', 'submitted', 'attachments', 'state', 'receipt',]

    def create(self, validated_data):
        attachments = validated_data.pop('attachments')
        kwargs = self.context['request'].resolver_match.kwargs

        std_id = self.context['request'].user.id
        if 'student' in validated_data:
            std_id = validated_data['std_id']

        Submission.objects.filter(part_id=kwargs['part_id'], student_id=std_id).update(current_submission=False)
        obj = Submission.objects.create(
            part_id=kwargs['part_id'], student_id=std_id, submitted=now(),
            **validated_data)
        for pk in attachments:
            attachment = SubmissionAttachment.objects.get(pk=pk)
            if attachment.submission_id:
                raise serializers.ValidationError('Submission Attachment already used')
            attachment.submission = obj
            attachment.save()
        return obj


class SubmissionAttachmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubmissionAttachment
        fields = ['id', 'attachment']

    def create(self, validated_data):
        return SubmissionAttachment.objects.create(
            user=self.context['request'].user, **validated_data)


class MarkSerializer(serializers.ModelSerializer):
    comments = serializers.JSONField()

    class Meta:
        model = Mark
        read_only_fields = ['mark_type', 'marker',  ]
        fields = [
            'id', 'coursework', 'student', 'mark', 'mark_type',
            'marker', 'comments', 'returned', ]

    def create(self, validated_data):
        mark_type = Mark.MARK_PASS
        cw = validated_data['coursework']
        mark_p = (validated_data['mark'] / cw.max_mark) * 100
        if mark_p < cw.mark_scheme.pass_mark_percentage:
            mark_type = Mark.MARK_FAIL

        return Mark.objects.create(
            marker=self.context['request'].user,
            mark_type=mark_type,
            **validated_data)
