function padNum(n){
    if(n.toString().length == 1){
        return `0${n}`;
    }
    return n;
}

let monthStrings = [
    'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
]

$(".date-control").each(function(){
    let input = this;
    let wrap = $("<div class='date-picker grid-row' aria-hidden='true'>").hide().insertAfter(this);
    let col = $("<div class='column-one-half calendar'>").appendTo(wrap);
    let controls = $("<div class='controls'>").appendTo(col);
    let prevMonth = $("<a class='button'>").text("Previous Month").appendTo(controls);
    let monthLabel = $("<span class='month'>").text("???").appendTo(controls);
    let nextMonth = $("<a class='button'>").text("Next Month").appendTo(controls);

    let days = $("<div class='dates'>").appendTo(col);

    let fillMonth = function(year, month){
        days.empty();

        // Previous month
        let first = new Date(year, month, 1);
        let start = first.getDay() - 1;
        for(let i = 0; i < start; i++){
            first.setDate(first.getDate() - 1); // Rollback
            $("<span class='previous-month date'>").text(first.getDate()).attr(
                "data-date", `${first.getFullYear()}-${padNum(first.getMonth())}-${padNum(first.getDate())}`).prependTo(days);
        }

        // Current Month
        let total = new Date(year, month, 1);
        total.setMonth(total.getMonth() + 1);
        total.setDate(0); // Rollback to end of month
        total_days = total.getDate();

        for(let i = 0; i < total_days; i++){
            $("<span class='date'>").text(i+1).attr(
                "data-date", `${total.getFullYear()}-${padNum(total.getMonth())}-${padNum(i+1)}`).appendTo(days);
        }

        // Next Month
        let end = total.getDay() - 1;
        for(let i = end; i < 6; i++){
            total.setDate(total.getDate() + 1); // Roll
            $("<span class='next-month date'>").text(total.getDate()).attr(
                "data-date", `${total.getFullYear()}-${padNum(total.getMonth())}-${padNum(total.getDate())}`).appendTo(days);
        }

        // Final linkup
        monthLabel.text(`${monthStrings[month]} ${year}`);
        $(".date", days).click(function(){
            $(input).val($(this).attr("data-date") + " 23:59");
        });
    };

    let today = new Date();
    fillMonth(today.getFullYear(), today.getMonth());

    prevMonth.on("click", function(){
        today.setMonth(today.getMonth() - 1);
        fillMonth(today.getFullYear(), today.getMonth());
        $(input).focus();
    });

    nextMonth.on("click", function(){
        today.setMonth(today.getMonth() + 1);
        fillMonth(today.getFullYear(), today.getMonth());
        $(input).focus();
    });

    let cT = null;
    $(this).on('focus', function(){
        wrap.show();
        clearTimeout(cT);
    }).on("blur", function(){
        cT = setTimeout(function(){
            wrap.hide();
        }, 300);
    });
});
