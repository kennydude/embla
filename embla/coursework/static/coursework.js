validate.extend(validate.validators.datetime, {
  // The value is guaranteed not to be null or undefined but otherwise it
  // could be anything.
  parse: function(value, options) {
    return +moment.utc(value);
  },
  // Input is a unix timestamp
  format: function(value, options) {
    var format = "YYYY-MM-DD hh:mm";
    return moment.utc(value).format(format);
  }
});

function serializeForm(){
    // Serialize form
    let form = {};
    $("input[name], select[name]").each(function(){
        if($(this).attr("type") == "radio" && !$(this).is(":checked")){
            return;
        }
        form[$(this).attr("name")] = $(this).val();
    });
    if(form['description_is_link'] == "True"){
        form['description'] = $("#link_description").val();
    } else{
        form['description'] = $("#md_description").val();
    }
    form['parts'] = [];
    $(".submission-parts li").each(function(){
        let part = {
            "details": {}
        };

        let setPart = function(key, value){
            if(key.indexOf("details.") === 0){
                part['details'][key.substr(8)] = value;
            } else{
                part[key] = value;
            }
        }

        $(".part-control", this).each(function(){
            setPart($(this).attr("data-name"), $(this).val());
        });
        $(".string-array", this).each(function(){
            let val = [];
            $("input", this).each(function(){
                if($(this).val() != "") val.push($(this).val());
            });
            setPart($(this).attr("data-name"), val);
        });

        form['parts'].push(part);
    });
    return form;
}

function validateForm(data, context){
    if (! validateBasics(data)) return false;
    if(context != "click-dates"){
        if (! validateDates(data)) return false;
    }
    return true;
}

function errorBox(errors, box){
    let b = $("<div class='error-summary' role='group'>").appendTo(box);
    $("<h1 class='heading-medium error-summary-heading'>").text(
        "This page contains some errors").appendTo(b);
    let l = $("<ul>").appendTo(b);
    for(key in errors){
        v = errors[key];
        if(Array.isArray(v)){
            v = v.join("<br/>");
        }
        $("<li>").html(v).appendTo(l);
    }
}

function doErrors(errors, key){
    $(`.${key}-tab .markers`).remove();
    $(`.${key}-error-box`).html("");

    if(errors != undefined){
        $("<div class='markers'>").html($(".invalid-tab-template").html()).appendTo(`.${key}-tab`);
        errorBox(errors, `.${key}-error-box`);
    } else{
        $("<i class='icon icon-tick markers'>").appendTo(`.${key}-tab a`);
        return true;
    }
}

function validateBasics(data){
    let validators = {
        title: {presence: true},
        mark_scheme: {presence: true},
        max_mark: {presence: true},
        owner: {presence: true},
        description: {
            presence: true,
            format: function(value, attributes, attributeName, options, constraints) {
                if(attributes['description_is_link'] != "False"){
                    return {
                        pattern: /http(s?)\:\:\/\/.+/
                    }
                }
                return false;
            }
        },
        description_is_link: {presence: true},
    };
    let errors = validate(data, validators);
    return doErrors(errors, "basic-details");   
}

function validateDates(data){
    let after = function(key){
        return function(value, attributes, attributeName, options, constraints) {
            return {
                earliest: attributes[key],
                message: `must be after ${validate.prettify(key)}`
            };
        }
    };

    let validators = {
        visible: {
            presence: true,
            datetime: true
        },
        submissions_open: {
            presence: true,
            datetime: after('visible')
        },
        due: {
            presence: true,
            datetime: after('submissions_open')
        },
        submissions_close: {
            presence: true,
            datetime: after('due')
        },
        mark_by: {
            presence: true,
            datetime: after('submissions_close')
        },
        returned: {
            presence: true,
            datetime: after('mark_by')
        }
    };
    let errors = validate(data, validators);
    return doErrors(errors, "dates");
}

$(".coursework-form").on("submit", function(e){
    e.preventDefault();
    let form = serializeForm();

    $.ajax({
        url: document.location.toString(),
        method: "POST",
        data: JSON.stringify(form),
        processData: false,
        contentType: 'application/json',
        success: function(data){
            document.location.href = "/modules/" + data['module_id'] + "/coursework/" + data['id'] + "/edit/";
        }
    });
});

// Coursework parts
$(".add-submission-part").click(function(){
    let template = $(".submission-part-template").html().replace(
        /\{\{ id \}\}/g, Math.floor((Math.random() * 100000) + 1));
    let part = $("<li>").html(template).appendTo(".submission-parts");
    processPart(part);
    run(part);
});
$(".submission-part-template").hide();

function processPart(part){
    $(".submission-type", part).on("change", function(){
        $(".online-submission-only, .offline-submission-only, .slot-submission-only", part).hide();
        if($(this).val() == "online"){
            $(".online-submission-only", part).show();
        } else if($(this).val() == "offline"){
            $(".offline-submission-only", part).show();
        } else if($(this).val() == "slot"){
            $(".slot-submission-only", part).show();
        }
    }).trigger("change");
    $(".remove-part", part).click(function(){
        if(confirm("Remove this submission part?")){
            part.remove();
        }
    });
}

$(".submission-parts li").each(function(){
    processPart(this);
});

$(window).on("resize", function(){
    $(".admin-sidebar").css("height", window.innerHeight + "px");
}).on("scroll", function(){
    $(".admin-sidebar").css("margin-top", Math.max(0, 114 - window.scrollY) + "px");
}).trigger("resize").trigger("scroll");

$(".column-three-quarters").css({
    'margin-left': '25%'
});
$(".admin-sidebar").css({
    'position': 'fixed',
    'width': '24%',
    'top': '0px'
});

// Tabs
$(".tab-contents .tab").hide();
$(".trigger-tab").on("click fakeclick", function(){
    $(".tab-contents .tab").hide();
    $(".admin-sidebar li").removeClass("active");
    $(".admin-sidebar [data-tab=" + $(this).data("tab") + "]").parent().addClass("active");
    $(".tab-contents .tab[data-tab=" + $(this).data("tab") + "]").show();
});
$($(".trigger-tab")[0]).trigger('fakeclick');

$(".trigger-tab").on("click", function(){
    validateForm(serializeForm(), `click-${$(this).data("tab")}`);
});
