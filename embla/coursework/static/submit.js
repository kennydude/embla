function resetDropzone(){
    $(".drop-zone .normal").show();
    $(".drop-zone .uploading").hide();
    $(".file-upload").val("");
}

function uploadFile(file){
    // Show spinner
    $(".drop-zone .normal").hide();
    $(".drop-zone .uploading").show();
    // Upload
    let filename = file.name;
    let data = new FormData();
    data.append("attachment", file);
    $.ajax({
        method: "POST",
        url: $(".drop-zone").data("url"),
        data: data,
        contentType: false,
        processData: false,
        success: function(data){
            resetDropzone();
            let file = $("<div>").html($(".file-template").html()).insertBefore(".drop-zone");
            $(".file-id", file).val(data['id']);
            $(".file-title", file).text(filename);
            $(".delete", file).click(function(){
                // File will be swept by background job
                if(confirm("Remove file?")){
                    $(this).closest(".file").remove();
                }
            });
        },
        error: function(){
            resetDropzone();
            alert("Upload failed. Please retry");
        }
    })
}

function doDropzone(){
    console.log("dropzone");
    var $drop = $(".drop-zone");

    $(".file-upload").on("change", function(e){
        uploadFile(e.target.files[0]);
    });

    $drop.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
        e.preventDefault();
        e.stopPropagation();
    })
    .on('dragover dragenter', function() {
        $drop.addClass('active');
    })
    .on('dragleave dragend drop', function() {
        $drop.removeClass('active');
    })
    .on('drop', function(e) {
        $.each(e.originalEvent.dataTransfer.files, function(k, file){
            uploadFile(file);
        });
    });
}
doDropzone();

$(document).on("run", function(){
    doDropzone();
});
