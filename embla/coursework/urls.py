from django.conf.urls import url

from embla.coursework.views import (
    CourseworkPartViewSet, SubmissionAttachmentViewSet, MarkViewSet,
    CourseworkViewSet, CourseworkEditViewSet, CourseworkSubmissionViewSet,
    MarkReleaseView)

urlpatterns = [
    url(r'^modules/(?P<module_id>[0-9]+)/coursework/$',
        CourseworkViewSet.as_view({
            'get': 'list'
    }), name='coursework-list'),
    url(r'^modules/(?P<module_id>[0-9]+)/coursework/(?P<coursework_id>[0-9]+)/parts/$',
        CourseworkPartViewSet.as_view({
            'get': 'list',
        }), name='coursework-part-list'),
    url(r'^modules/(?P<module_id>[0-9]+)/coursework/(?P<coursework_id>[0-9]+)/submissions/$',
        CourseworkSubmissionViewSet.as_view({
            'get': 'list',
        }), name='coursework-submissions'),
    url(r'^modules/(?P<module_id>[0-9]+)/coursework/(?P<coursework_id>[0-9]+)/parts/(?P<part_id>[0-9]+)/submit/$',
        CourseworkSubmissionViewSet.as_view({
            'get': 'new_form',
            'post': 'create',
        }), name='coursework-part-submit'),
    url(r'^modules/(?P<module_id>[0-9]+)/coursework/(?P<pk>[0-9]+)/$',
        CourseworkViewSet.as_view({
            'get': 'retrieve',
        }, template_name='coursework/view.html'), name='coursework-view'),
    url(r'^modules/(?P<module_id>[0-9]+)/coursework/(?P<coursework_id>[0-9]+)/mark/$',
        MarkViewSet.as_view({
            'get': 'create_view',
            'post': 'create',
        }, template_name='coursework/mark_online.html'), name='coursework-mark'),
    url(r'^modules/(?P<module_id>[0-9]+)/coursework/(?P<pk>[0-9]+)/edit/$',
        CourseworkEditViewSet.as_view({
            'get': 'retrieve',
            'post': 'update',
        }), name='coursework-edit-form'),
    url(r'^modules/(?P<module_id>[0-9]+)/coursework/(?P<coursework_id>[0-9]+)/mark/release/$',
        MarkReleaseView.as_view(), name='coursework-mark-release'),
    url(r'^modules/(?P<module_id>[0-9]+)/coursework/new/$',
        CourseworkEditViewSet.as_view({
            'get': 'new_form',
            'post': 'create',
    }), name='coursework-new-form'),
    url(r'^submissions/attachments/$',
        SubmissionAttachmentViewSet.as_view({
            'post': 'create',
    }), name='coursework-attachments'),
]
