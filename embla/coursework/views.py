from django.shortcuts import get_object_or_404

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from embla.base.models import Module, ModuleUser
from embla.base.serializers import ModuleSerializer, UserSerializer
from embla.base.mixins import RetrieveMetadaMixin
from embla.coursework.models import (
    Coursework, MarkScheme, Submission, CourseworkPart, SubmissionAttachment, Mark)
from embla.coursework.serializers import (
    CourseworkSerializer, MarkSchemeSerializer, FullCourseworkSerializer, SubmissionSerializer,
    CourseworkPartSerializer, SubmissionAttachmentSerializer, MarkSerializer)


class CourseworkViewSet(RetrieveMetadaMixin, viewsets.ModelViewSet):
    serializer_class = CourseworkSerializer
    template_name = 'coursework/list.html'

    def initial(self, request, *args, **kwargs):
        super(CourseworkViewSet, self).initial(request, *args, **kwargs)
        self.module = Module.objects.get(pk=self.kwargs['module_id'])

    def get_queryset(self):
        return Coursework.objects.filter(
            module=self.module)

    def get_metadata(self):
        return {
            'module': ModuleSerializer(self.module, context={ 'request': self.request }).data
        }


class CourseworkEditViewSet(CourseworkViewSet):
    template_name = 'coursework/new.html'
    serializer_class = FullCourseworkSerializer

    def get_metadata(self):
        o = {
            'users': UserSerializer(
                [x.user for x in ModuleUser.objects.filter(
                    module=self.module,
                    relationship=ModuleUser.RELATIONSHIP_STAFF)],
                context={'request': self.request}, many=True).data,
            'mark_schemes': MarkSchemeSerializer(
                MarkScheme.objects.filter(organisation=self.request.organisation),
                many=True).data
        }
        o.update(super(CourseworkEditViewSet, self).get_metadata())
        return o

    def new_form(self, request, module_id):
        return self.paginator.get_paginated_response({}, self)


class CourseworkPartViewSet(RetrieveMetadaMixin, viewsets.ModelViewSet):
    serializer_class = CourseworkPartSerializer
    template_name = 'coursework/part_list.html'

    def get_queryset(self):
        return CourseworkPart.objects.filter(
            coursework_id=self.kwargs['coursework_id'])

    def get_metadata(self):
        context = {'request': self.request}
        coursework = get_object_or_404(Coursework, pk=self.kwargs['coursework_id'])
        return {
            'coursework': CourseworkSerializer(coursework, context=context).data,
            'module': ModuleSerializer(coursework.module, context=context).data,
        }


class CourseworkSubmissionViewSet(RetrieveMetadaMixin, viewsets.ModelViewSet):
    queryset = Submission.objects.all()
    template_name = 'coursework/submit.html'
    serializer_class = SubmissionSerializer

    def get_queryset(self):
        return Submission.objects.filter(
            current_submission=True,
            part__coursework_id=self.kwargs['coursework_id'])

    def get_metadata(self):
        if 'part_id' in self.kwargs:
            part = get_object_or_404(CourseworkPart,
                pk=self.kwargs['part_id'], coursework_id=self.kwargs['coursework_id'])
            context = {'request': self.request}
            return {
                'part': CourseworkPartSerializer(part, context=context).data,
                'coursework': CourseworkSerializer(part.coursework, context=context).data,
                'module': ModuleSerializer(part.coursework.module, context=context).data,
                'previous_submission': Submission.objects.filter(
                    student=self.request.user, part=part).exists()
            }

    def new_form(self, request, module_id, coursework_id, part_id):
        return self.paginator.get_paginated_response({}, self)

    def create(self, *args, **kwargs):
        response = super(CourseworkSubmissionViewSet, self).create(*args, **kwargs)
        response.template_name = 'coursework/submission.html'
        response.data['meta'] = self.get_metadata()
        return response


class SubmissionAttachmentViewSet(viewsets.ModelViewSet):
    serializer_class = SubmissionAttachmentSerializer
    queryset = SubmissionAttachment.objects.none()


class MarkViewSet(viewsets.ModelViewSet):
    queryset = Mark.objects.all()
    serializer_class = MarkSerializer
    template_name = 'coursework/mark_online.html'

    def create_view(self, request, *args, **kwargs):
        return Response({
            'meta': self.get_metadata(),
            'id': self.kwargs['coursework_id']
        })

    def get_metadata(self):
        context = {'request': self.request}
        coursework = get_object_or_404(Coursework, pk=self.kwargs['coursework_id'])
        return {
            'coursework': CourseworkSerializer(coursework, context=context).data,
            'module': ModuleSerializer(coursework.module, context=context).data,
        }


class MarkReleaseView(APIView):
    template_name = 'coursework/release.html'

    def get(self, request, *args, **kwargs):
        context = {'request': self.request}
        self.coursework = get_object_or_404(Coursework, pk=self.kwargs['coursework_id'])
        data = CourseworkSerializer(self.coursework, context=context).data
        data['module'] = ModuleSerializer(self.coursework.module, context=context).data
        return Response(data)

    def post(self, request, *args, **kwargs):
        r = self.get(request)
        Mark.objects.filter(coursework=self.coursework).update(
            returned=True)
        return r
