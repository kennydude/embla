from django.contrib import admin

from embla.feed.models import FeedAttachment, FeedLink, FeedItem


@admin.register(FeedAttachment)
class AttachmentAdmin(admin.ModelAdmin):
    list_display = ['id', 'attachment', 'item']


@admin.register(FeedLink)
class LinkAdmin(admin.ModelAdmin):
    list_display = ['item', 'title']


@admin.register(FeedItem)
class ItemAdmin(admin.ModelAdmin):
    list_display = ['module', 'poster', 'posted', ]
