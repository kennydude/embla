from django.db import models
from django.conf import settings
from django.utils.timezone import now


class FeedItem(models.Model):
    module = models.ForeignKey('base.Module')
    poster = models.ForeignKey(settings.AUTH_USER_MODEL)
    comment = models.TextField()
    visible_students = models.BooleanField(default=True)
    posted = models.DateTimeField(default=now)

    def __str__(self):
        return '{} {} {}'.format(self.id, self.module, self.posted)


class FeedAttachment(models.Model):
    attachment = models.FileField()
    item = models.ForeignKey(
        FeedItem, null=True, related_name='attachments')

    def __str__(self):
        return str(self.attachment)


class FeedLink(models.Model):
    link = models.URLField()
    title = models.CharField(max_length=200)
    item = models.ForeignKey(FeedItem, related_name='links')

    def __str__(self):
        return self.title
