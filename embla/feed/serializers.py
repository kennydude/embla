from rest_framework import serializers

from embla.feed.models import FeedAttachment, FeedItem, FeedLink


class FeedAttachmentSerializer(serializers.ModelSerializer):
    filename = serializers.SerializerMethodField()

    def get_filename(self, obj):
        return obj.attachment.name

    class Meta:
        model = FeedAttachment
        fields = ['attachment', 'filename', ]

    def create(self, validated_data):
        return FeedAttachment.objects.create(**validated_data)


class FeedLinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = FeedLink
        fields = ['link', 'title', ]


class FeedSerializer(serializers.ModelSerializer):
    attachments = FeedAttachmentSerializer(many=True)
    links = FeedLinkSerializer(many=True)

    class Meta:
        model = FeedItem
        fields = [
            'id', 'comment', 'visible_students', 'poster', 'module',
            'attachments', 'links', 'posted', ]
        read_only_fields = ['poster', 'module', 'posted', ]

    def create(self, validated_data):
        attachments = validated_data.pop('attachments')
        links = validated_data.pop('links')  # TODO
        kwargs = self.context['request'].resolver_match.kwargs

        user_id = self.context['request'].user.id

        obj = FeedItem.objects.create(
            module_id=kwargs['module_id'], poster_id=user_id,
            **validated_data)
        for pk in attachments:
            attachment = FeedAttachment.objects.get(pk=pk)
            if attachment.item_id:
                raise serializers.ValidationError('Attachment already used')
            attachment.item = obj
            attachment.save()
        return obj
