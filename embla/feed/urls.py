from django.conf.urls import url

from embla.feed.views import FeedViewSet, AttachmentViewSet

urlpatterns = [
    url(r'^modules/(?P<module_id>[0-9]+)/feed/$', FeedViewSet.as_view({
        'get': 'list',
        'post': 'create',
    }), name='feed'),
    url(r'^feed/attachments/$', AttachmentViewSet.as_view({
        'post': 'create'
    }), name='feed-attachment'),

    url(r'^feed/(?P<pk>[0-9])/visibility/$', FeedViewSet.as_view({
        'post': 'set_visibility'
    }), name='feed-visibility'),

]
