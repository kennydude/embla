from django.shortcuts import get_object_or_404

from rest_framework.response import Response
from rest_framework import viewsets

from embla.base.models import Module, ModuleUser
from embla.base.serializers import ModuleSerializer
from embla.feed.serializers import FeedSerializer, FeedAttachmentSerializer
from embla.feed.models import FeedItem, FeedAttachment


class FeedViewSet(viewsets.ModelViewSet):
    """Feed viewset
    """
    template_name = 'feed/list.html'
    serializer_class = FeedSerializer

    def get_metadata(self):
        context = {'request': self.request}
        module = Module.objects.get(pk=self.kwargs['module_id'])
        return {
            'module': ModuleSerializer(module, context=context).data,
        }

    def get_queryset(self):
        qs = FeedItem.objects.filter(module_id=self.kwargs['module_id'])
        if (ModuleUser.objects.get(
                user=self.request.user,
                module_id=self.kwargs['module_id']).relationship == ModuleUser.RELATIONSHIP_STUDENT):
            qs = qs.filter(visible_students=True)
        return qs

    def create(self, *args, **kwargs):
        response = super(CourseworkSubmissionViewSet, self).create(*args, **kwargs)
        response.template_name = 'feed/single.html'
        response.data['meta'] = self.get_metadata()
        return response

    def set_visibility(self, request, pk):
        item = get_object_or_404(FeedItem, pk=pk)
        item.visible_students = request.POST.get("visibility", "invisible") == "visible"
        item.save()
        return Response({}, template_name='feed/visible.html')


class AttachmentViewSet(viewsets.ModelViewSet):
    serializer_class = FeedAttachmentSerializer
    queryset = FeedAttachment.objects.none()
