from django.contrib import admin

from embla.tasks.models import Task


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ['for_user', 'task_type', 'id', ]
