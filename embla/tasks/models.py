import arrow

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now

from embla.base.fields import KeyValueField
from embla.coursework.models import Coursework


class Task(models.Model):
    """Task model. Changes to other models may cause these to be deleted or changed
    """

    TASK_COURSEWORK = 'coursework'
    TASK_LECTURE_SLIDES = 'lecture'

    TASK_TYPES = (
        (TASK_COURSEWORK, _('Coursework Due')),
        (TASK_LECTURE_SLIDES, _('Lecture Slides Upload Task')),
    )

    for_user = models.ForeignKey(settings.AUTH_USER_MODEL)
    task_type = models.CharField(max_length=10, choices=TASK_TYPES)
    details = KeyValueField(default={})
    expires = models.DateTimeField()
    visible = models.DateTimeField(default=now)

    def coursework(self):
        cwid = self.details.get("coursework", None)
        if cwid:
            return Coursework.objects.get(pk=cwid)
        else:
            return None

    def module(self):
        cw = self.coursework()
        if cw:
            return cw.module
        else:
            return None

    def is_important(self):
        return arrow.get().replace(days=-4) > self.expires
