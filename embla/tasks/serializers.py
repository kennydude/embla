from rest_framework import serializers

from embla.tasks.models import Task
from embla.base.serializers import ModuleSerializer
from embla.coursework.serializers import CourseworkSerializer


class TaskSerializer(serializers.ModelSerializer):
    coursework = CourseworkSerializer()
    module = ModuleSerializer()
    expires_nice = serializers.DateTimeField(
        format='%d %b', read_only=True)

    class Meta:
        model = Task
        fields = [
            'id', 'task_type', 'details', 'expires', 'coursework',
            'module', 'is_important', 'expires_nice', ]
