from django.conf.urls import url

from embla.tasks.views import TaskViewSet


urlpatterns = [
    url(r'^tasks/$', TaskViewSet.as_view({
        'get': 'list'
    }), name='task-list'),
]
