from django.utils.timezone import now

from rest_framework import viewsets

from embla.tasks.models import Task
from embla.tasks.serializers import TaskSerializer


class TaskViewSet(viewsets.ModelViewSet):
    serializer_class = TaskSerializer
    template_name = 'tasks/list.html'

    def get_queryset(self):
        return Task.objects.filter(
            visible__lte=now(),
            expires__gte=now(),
            for_user=self.request.user)
