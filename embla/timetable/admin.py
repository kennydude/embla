from django.contrib import admin

from embla.timetable.models import *


@admin.register(TimetableEvent)
class TimetableAdmin(admin.ModelAdmin):
    pass
