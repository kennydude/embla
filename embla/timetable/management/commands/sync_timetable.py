from django.core.management.base import BaseCommand, CommandError

import requests
import arrow

from embla.base.models import Module, Year, Organisation
from embla.timetable.models import TimetableEvent


class Command(BaseCommand):
    help = 'Syncs timetable with m.ncl.ac.uk'

    def handle(self, *args, **options):
        """Handle sync globally
        """
        for org in Organisation.objects.all():
            self.stdout.write('> Syncing timetable for {}'.format(org))
            self.handle_organisation(org)

    def handle_organisation(self, org):
        """Process everything needed for an organisation to sync
        """
        year = Year.objects.get_current(org)
        for module in Module.objects.filter(year=year):
            self.stdout.write('>> Syncing timetable for {}'.format(module))
            ts = org.config.get('timetable_service')
            if hasattr(self, 'handle_{}'.format(ts)):
                getattr(self, 'handle_{}'.format(ts))(org, module)
            else:
                self.stderr.write('>> Failure. {} timetable service not handled'.format(ts))

    def handle_xssTTActivitiesProxy(self, org, module):
        """Designed to handle the activities proxy on m.ncl.ac.uk
        """
        # Process week list first
        weeks_raw = requests.get("{}xssTTWeeksProxy.js".format(org.config.get("timetable_url")))
        weeks = {}
        for week in weeks_raw.json()['Data']['weeks']['week']:
            weeks[week.get('Week')] = week.get('WeekStart')

        req = requests.get("{}xssTTActivitiesLocationProxy.php".format(
            org.config.get("timetable_url")), params={
                'endpoint': 'Modules',
                'modules': module.code,
            })
        data = req.json()
        translator = {
            'B': TimetableEvent.EVENT_BOOKING,
            'C': TimetableEvent.EVENT_COMPUTER,
            'F': TimetableEvent.EVENT_FIELDWORK,
            'H': TimetableEvent.EVENT_CLINIC,
            'L': TimetableEvent.EVENT_LECTURE,
            'P': TimetableEvent.EVENT_PRACTICAL,
            'S': TimetableEvent.EVENT_SEMINAR,
            'T': TimetableEvent.EVENT_TUTORIAL,
            'V': TimetableEvent.EVENT_FILM,
            'W': TimetableEvent.EVENT_WORKSHOP,
        }

        TimetableEvent.objects.filter(
            module=module, event_source=TimetableEvent.SOURCE_SERVICE).delete()

        activites = {}
        for activity in data['Data']['Acts']['Act']:
            activites[activity.get('HostKey')] = activity

        events = []
        for time in data['Data']['WeekAndDays']['WeekAndDay']:
            activity = activites[time.get('Activity_HostKey')]
            # Figure out what day it runs on
            date = arrow.get(
                weeks[time.get('TimetableWeek')], 'DD/MM/YYYY').replace(
                    days=int(time.get('DayInWeek'))).format('YYYY-MM-DD')

            # Now add time
            start = arrow.get(''.join([date, activity.get('ScheduledStartTime')[10:]]))
            end = arrow.get(''.join([date, activity.get('ScheduledEndTime')[10:]]))

            # Stage the event to be added to DB
            events.append(TimetableEvent(
                module=module,
                event_type=translator[activity.get('ActivityType_Name')],
                event_source=TimetableEvent.SOURCE_SERVICE,
                start=start.datetime,
                end=end.datetime))
        TimetableEvent.objects.bulk_create(events)

