from django.db import models
from django.utils.translation import ugettext_lazy as _

import arrow

from embla.base.models import Module


class TimetableEvent(models.Model):
    """An event in the timetable
    """
    EVENT_LECTURE = 'lecture'
    EVENT_SEMINAR = 'seminar'
    EVENT_PRACTICAL = 'practical'
    EVENT_TUTORIAL = 'tutorial'
    EVENT_COMPUTER = 'computer'
    EVENT_WORKSHOP = 'workshop'
    EVENT_CLINIC = 'clinic'
    EVENT_FIELDWORK = 'field'
    EVENT_FILM = 'film'
    EVENT_BOOKING = 'book'

    EVENT_TYPES = (
        (EVENT_LECTURE, _('Lecture')),
        (EVENT_SEMINAR, _('Seminar')),
        (EVENT_PRACTICAL, _('Practical')),
        (EVENT_TUTORIAL, _('Tutorial')),
        (EVENT_COMPUTER, _('Comp Class')),
        (EVENT_WORKSHOP, _('Workshop')),
        (EVENT_CLINIC, _('Clinic')),
        (EVENT_FIELDWORK, _('Fieldwork')),
        (EVENT_FILM, _('Film Showing')),
        (EVENT_BOOKING, _('Booking')),
    )

    SOURCE_SERVICE = 'service'
    SOURCE_MANUAL = 'manual'

    EVENT_SOURCES = (
        (SOURCE_SERVICE, _('Automatically added via Timetable Service')),
        (SOURCE_MANUAL, _('Manaully added')),
    )

    event_type = models.CharField(max_length=20, choices=EVENT_TYPES)
    event_source = models.CharField(max_length=20, choices=EVENT_SOURCES, default=SOURCE_MANUAL)

    module = models.ForeignKey(Module)
    start = models.DateTimeField()
    end = models.DateTimeField()

    def start_time(self):
        return self.start.strftime('%H:%M')

    def has_passed(self):
        return arrow.get() > arrow.get(self.start)

    def __str__(self):
        return '{} {} from {} until {}'.format(self.module, self.event_type, self.start, self.end)
