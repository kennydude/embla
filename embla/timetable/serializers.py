from rest_framework import serializers

from embla.timetable.models import TimetableEvent


class TimetableSerializer(serializers.ModelSerializer):
    module = serializers.CharField(source='module.name')
    module_code = serializers.CharField(source='module.code')

    class Meta:
        model = TimetableEvent
        fields = [
            'event_type', 'module', 'module_code', 'start', 'end', 'start_time',
            'has_passed']
