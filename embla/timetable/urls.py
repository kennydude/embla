from django.conf.urls import url

from embla.timetable.views import TimetableViewSet

urlpatterns = [
    url(r'^timetable/$', TimetableViewSet.as_view({
        'get': 'list'
    }), name='timetable'),
]
