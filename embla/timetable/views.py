import datetime

from rest_framework import viewsets
import django_filters

from embla.base.models import Year
from embla.timetable.models import TimetableEvent
from embla.timetable.serializers import TimetableSerializer


class TimetableFilterSet(django_filters.FilterSet):
    current_date = django_filters.BooleanFilter(method='filter_current_date')

    class Meta:
        model = TimetableEvent
        fields = ['current_date']

    def filter_current_date(self, queryset, name, value):
        if value:
            today = datetime.date.today()
            queryset = queryset.filter(start__date=today)
        return queryset


class TimetableViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = TimetableSerializer
    template_name = 'timetable/events.html'
    filter_class = TimetableFilterSet

    def get_queryset(self):
        return TimetableEvent.objects.filter(
            module__year=Year.objects.get_current(self.request.organisation),
            module__users__in=[self.request.user]).order_by('-start')
