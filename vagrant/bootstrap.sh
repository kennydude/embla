sudo yum install -y epel-release
sudo yum install -y python34 python-pip nano
sudo yum install -y postgresql-server

sudo cp /vagrant/vagrant/pg_hba.conf /var/lib/pgsql/data/pg_hba.conf

sudo postgresql-setup initdb
sudo service postgresql start

if sudo -u postgres bash -c "psql -c \"CREATE USER embla WITH PASSWORD 'password' CREATEDB;\""
then echo "embla user created"
fi

if sudo -u postgres bash -c "psql -c \"ALTER USER embla WITH SUPERUSER;\""
then echo "embla user made into superuser"
fi

if sudo -u postgres createdb -E UTF8 -T template0 -O embla embla
then echo "embla db created"
fi

cp /vagrant/vagrant/local_settings.py /vagrant/embla/settings/local.py

pip install -U pip
pip install virtualenv

cd /vagrant

virtualenv -p python3 venv
. venv/bin/activate